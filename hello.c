//file aa.c
#include <stdio.h>

void foo() __attribute__((constructor));
void bar() __attribute__((destructor));

void my_atexit(void)
{
	printf("Hello from %s\n", "my_atexit");
}

int main()
{
	atexit(my_atexit);
	return 0;
}

void foo() { printf("Hello from %s\n", "foo"); }
void bar() { printf("Hello from %s\n", "bar"); }


