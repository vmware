/* filename :aa.c */
#include <stdio.h>

int main(void)
{
	unsigned long long tmp = 2;

	/* 下面这样改成asm volatile("addq $0x7fffffff, %0" 就可以 */
	//asm volatile("movq $0x1122334455667788, %0"
	asm volatile("addq $0x1122334455667788, %0"
		     : "=a" (tmp)
		     : "0" (tmp)
		    );

	printf("tmp is %p\n", tmp);

	return 0;
}

